# 贡献导引

如果你正要提交的文档是 **当前稳定版本**，请提交至对应的分支，例如，Laravel 5.1 的文档需要提交至 `5.1` 分支，若是要提交下一个 Laravel 版本，请提交至 `master` 分支。





--- 

> {note} 欢迎任何形式的转载，但请务必注明出处，尊重他人劳动共创开源社区。
> 
> 转载请注明：本文档由 Laravel China 社区 [laravel-china.org] 组织翻译。
> 
> 文档永久地址： http://d.laravel-china.org