
## 说明

台湾繁体转为中文的对应，注意，批量替换的部分替换要区分开，批量替换下次可以考虑使用程序来统一执行。

## 可批量替换词汇对应

* 变更|修改
* 终端机|命令行
* 链结|链式调用
* 扩充功能|扩展功能
* 基底模型|模型基类
* 覆写|重写
* 串行化|序列化
* 复写|重写
* 多笔|多个
* 例外|异常
* 全域|全局
* 运行档|执行文件
* 暂存|缓存
* 检阅|阅读
* 预留值|预设值
* 更多消息|更多信息
* 翻修|改进
* 回报|反馈
* 取得|获取
* 相依|依赖
* 语系|语言
* 获取的|获取到的
* 回应|响应
* 建置|构建
* 资源库|代码仓库
* 造访|访问
* 正规表达式|正则表达式
* 给定|指定
* 重新导向|重定位
* 注解|注释
* 产生|生成
* 语言包|语言包
* 网域|域名
* 单一|单个
* 建构器|构造器
* 现行|现有
* 样板|模版
* 高端|高级
* 惯例|约定
* 影片|图片
* 底线|下划线

## 部分替换

* 其他|其它
