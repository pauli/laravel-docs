<?php

return [
    [
        'text' => '翻译说明',
        'link' => '/docs/{{version}}/about'
    ],
    [
        'text' => '发行说明',
        'link' => '/docs/{{version}}/releases'
    ],
    [
        'text' => '升级导引',
        'link' => '/docs/{{version}}/upgrade'
    ],
    [
        'text' => '贡献导引',
        'link' => '/docs/{{version}}/contributions'
    ],
    [
        'text' => '安装',
        'link' => '/docs/{{version}}/installation'
    ],
    [
        'text' => 'Homestead',
        'link' => '/docs/{{version}}/homestead'
    ],
    [
        'text' => '路由',
        'link' => '/docs/{{version}}/routing'
    ],
    [
        'text' => '中间件',
        'link' => '/docs/{{version}}/middleware'
    ],
    [
        'text' => '控制器',
        'link' => '/docs/{{version}}/controllers'
    ],
    [
        'text' => '请求',
        'link' => '/docs/{{version}}/requests'
    ],
    [
        'text' => '响应',
        'link' => '/docs/{{version}}/responses'
    ],
    [
        'text' => '视图',
        'link' => '/docs/{{version}}/views'
    ],
    [
        'text' => 'Blade 模板',
        'link' => '/docs/{{version}}/blade'
    ],
    [
        'text' => '初级任务清单',
        'link' => '/docs/{{version}}/quickstart'
    ],
    [
        'text' => '中级任务清单',
        'link' => '/docs/{{version}}/quickstart-intermediate'
    ],
    [
        'text' => '请求的生命周期',
        'link' => '/docs/{{version}}/lifecycle'
    ],
    [
        'text' => '应用程序结构',
        'link' => '/docs/{{version}}/structure'
    ],
    [
        'text' => '服务提供者',
        'link' => '/docs/{{version}}/providers'
    ],
    [
        'text' => '服务容器',
        'link' => '/docs/{{version}}/container'
    ],
    [
        'text' => 'Contracts',
        'link' => '/docs/{{version}}/contracts'
    ],
    [
        'text' => 'Facades',
        'link' => '/docs/{{version}}/facades'
    ],
    [
        'text' => '用户认证',
        'link' => '/docs/{{version}}/authentication'
    ],
    [
        'text' => '用户授权',
        'link' => '/docs/{{version}}/authorization'
    ],
    [
        'text' => 'Artisan 命令行',
        'link' => '/docs/{{version}}/artisan'
    ],
    [
        'text' => '交易',
        'link' => '/docs/{{version}}/billing'
    ],
    [
        'text' => '缓存',
        'link' => '/docs/{{version}}/cache'
    ],
    [
        'text' => '集合',
        'link' => '/docs/{{version}}/collections'
    ],
    [
        'text' => 'Elixir',
        'link' => '/docs/{{version}}/elixir'
    ],
    [
        'text' => '加密与解密',
        'link' => '/docs/{{version}}/encryption'
    ],
    [
        'text' => '错误与日志',
        'link' => '/docs/{{version}}/errors'
    ],
    [
        'text' => '事件',
        'link' => '/docs/{{version}}/events'
    ],
    [
        'text' => '文件系统与云存储',
        'link' => '/docs/{{version}}/filesystem'
    ],
    [
        'text' => '哈希',
        'link' => '/docs/{{version}}/hashing'
    ],
    [
        'text' => '辅助函数',
        'link' => '/docs/{{version}}/helpers'
    ],
    [
        'text' => '本地化',
        'link' => '/docs/{{version}}/localization'
    ],
    [
        'text' => '邮件',
        'link' => '/docs/{{version}}/mail'
    ],
    [
        'text' => '扩展包开发',
        'link' => '/docs/{{version}}/packages'
    ],
    [
        'text' => '分页',
        'link' => '/docs/{{version}}/pagination'
    ],
    [
        'text' => '队列',
        'link' => '/docs/{{version}}/queues'
    ],
    [
        'text' => 'Redis',
        'link' => '/docs/{{version}}/redis'
    ],
    [
        'text' => 'Session',
        'link' => '/docs/{{version}}/session'
    ],
    [
        'text' => 'Envoy',
        'link' => '/docs/{{version}}/envoy'
    ],
    [
        'text' => '任务调度',
        'link' => '/docs/{{version}}/scheduling'
    ],
    [
        'text' => '测试',
        'link' => '/docs/{{version}}/testing'
    ],
    [
        'text' => '表单验证',
        'link' => '/docs/{{version}}/validation'
    ],
    [
        'text' => '数据库入门',
        'link' => '/docs/{{version}}/database'
    ],
    [
        'text' => '查询构造器',
        'link' => '/docs/{{version}}/queries'
    ],
    [
        'text' => '数据库迁移',
        'link' => '/docs/{{version}}/migrations'
    ],
    [
        'text' => '数据填充',
        'link' => '/docs/{{version}}/seeding'
    ],
    [
        'text' => 'Eloquent 入门',
        'link' => '/docs/{{version}}/eloquent'
    ],
    [
        'text' => '模型关联',
        'link' => '/docs/{{version}}/eloquent-relationships'
    ],
    [
        'text' => 'Eloquent 集合',
        'link' => '/docs/{{version}}/eloquent-collections'
    ],
    [
        'text' => 'Getter/Setter',
        'link' => '/docs/{{version}}/eloquent-mutators'
    ],
    [
        'text' => '序列化',
        'link' => '/docs/{{version}}/eloquent-serialization'
    ]
];